#![feature(proc_macro_hygiene, arbitrary_self_types)]

use std::rc::Rc;

use dominator::Dom;
use wasm_bindgen::JsCast;
use wasm_bindgen::prelude::*;
use web_sys;

use crate::state::*;
use crate::util::*;

pub mod api;
pub mod util;
pub mod state;


#[derive(Default)]
pub struct App {
    state: Rc<RootState>,
}

impl App {
    pub fn new(state: RootState) -> App {
        App {
            state: Rc::new(state),
        }
    }

    pub fn render(&self) -> Dom {
        let state = Rc::clone(&self.state);
        state.render()
    }

    pub fn handle_click(&self, event: web_sys::Event) {
        console_log("on click!");

        let target: web_sys::EventTarget = match event.target() {
            Some(target) => target,
            None => return,
        };

        let anchor: web_sys::HtmlAnchorElement = match target.dyn_into() {
            Ok(anchor) => anchor,
            Err(_) => return,
        };

        let location: web_sys::Location = window().location();
        let history: web_sys::History = window().history().unwrap();
        if anchor.origin() == location.origin().unwrap() {
            event.prevent_default();
            let path = anchor.pathname();
            self.state.set_path(path.clone());
            history.push_state_with_url(&JsValue::null(), "", Some(&path)).unwrap();
        }
    }

    pub fn handle_popstate(&self, _event: web_sys::PopStateEvent) {
        console_log("on popstate");

        let location: web_sys::Location = window().location();
        let path = format!("{}{}", location.pathname().unwrap(), location.search().unwrap());
        self.state.set_path(path);
    }
}

#[wasm_bindgen]
pub struct Client {
    #[allow(unused)]
    app: Rc<App>,
    _on_popstate: Closure<dyn FnMut(web_sys::PopStateEvent)>,
    _on_click: Closure<dyn FnMut(web_sys::Event)>,
}

#[wasm_bindgen]
impl Client {
    #[wasm_bindgen(constructor)]
    pub fn new() -> Client {
        console_error_panic_hook::set_once();

        let window = window();
        let document = window.document().unwrap();

        let mount_el = document.get_element_by_id("app_mount")
            .expect("Cannot find element with id 'app_mount'");
        let app = Rc::new(App::new(RootState::default()));

        // on_popstate
        let on_popstate = {
            let app = Rc::clone(&app);
            Closure::new(move |event: web_sys::PopStateEvent| {
                app.handle_popstate(event);
            })
        };
        window.set_onpopstate(Some(on_popstate.as_ref().unchecked_ref()));

        // on_click
        let on_click = {
            let app = Rc::clone(&app);
            Closure::new(move |event: web_sys::Event| {
                app.handle_click(event);
            })
        };
        window.add_event_listener_with_callback("click", on_click.as_ref().unchecked_ref()).unwrap();

        dominator::append_dom(&mount_el, app.render());

        Client {
            app,
            _on_popstate: on_popstate,
            _on_click: on_click,
        }
    }

    #[wasm_bindgen]
    pub fn set_path(&self, path: String) {
        self.app.state.set_path(path.clone());
    }
}
