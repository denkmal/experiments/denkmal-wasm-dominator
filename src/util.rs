use dominator::DomBuilder;
use wasm_bindgen::prelude::*;

pub fn window() -> web_sys::Window {
    web_sys::window().unwrap()
}

pub fn html(name: &str) -> DomBuilder<web_sys::HtmlElement> {
    DomBuilder::new_html(name)
}

pub fn console_log(text: &str) {
    web_sys::console::log_1(&text.into());
}

#[wasm_bindgen]
extern "C" {
    pub fn createSwiper();
    pub fn benchStart();
    pub fn benchStop();
}
