use std::rc::Rc;

use dominator::{Dom, events, text};
use futures_signals::signal::{Mutable, SignalExt};

use crate::api;
use crate::api::*;
use crate::util::*;

#[derive(Debug)]
pub struct Event {
    pub event: api::GetEventsRegionsEvents,
    pub starred: Mutable<bool>,
}

#[derive(Debug)]
pub struct EventSorter {
    pub event: Rc<Event>,
    pub starred: bool,
}

impl Event {
    pub fn toggle_starred(&self) {
        self.starred.replace_with(|&mut s| !s);
    }

    pub fn render(self: Rc<Event>) -> Dom {
        html("a")
            .class("Event")
            .attribute("href", "javascript:;")
            .style_signal("background", self.starred.signal().map(|starred| {
                if starred { "yellow" } else { "transparent" }
            }))
            .event({
                let event = Rc::clone(&self);
                move |_: events::Click| {
                    event.toggle_starred();
                }
            })
            .children(&mut [
                html("div")
                    .class("Event-meta")
                    .children(&mut [
                        html("div")
                            .class("Event-meta-venue")
                            .children(&mut [
                                html("span")
                                    .class("text-overflow-ellipsis")
                                    .text(&*self.event.venue.name)
                                    .into_dom(),
                            ])
                            .into_dom(),
                        html("div")
                            .class("Event-meta-time")
                            .children(&mut [
                                html("time")
                                    .text(&*self.event.from)
                                    .into_dom(),
                            ])
                            .into_dom(),
                    ])
                    .into_dom(),
                html("div")
                    .class("Event-description")
                    .children(&mut [
                        text(&*self.event.description),
                        html("span")
                            .class("Genres")
                            .children(self.event.genres.iter()
                                .map(|genre| {
                                    self.render_genre(genre)
                                }))
                            .into_dom(),
                    ])
                    .into_dom(),
            ])
            .into_dom()
    }

    fn render_genre(&self, genre: &GetEventsRegionsEventsGenres) -> Dom {
        let color = match &genre.category.color {
            Some(color) => format!("{}", color),
            None => "".to_string(),
        };
        let style = format!("background-image:linear-gradient(to top, #{} var(--genre-color-size), transparent var(--genre-color-size));", color);

        html("span")
            .class("Genres-tag")
            .children(&mut [
                html("span")
                    .class("Genres-tag-background")
                    .attribute("title", &*genre.name)
                    .attribute("style", &style)
                    .into_dom(),
                html("span")
                    .class("Genres-tag-label")
                    .text(&*genre.name)
                    .into_dom(),
            ])
            .into_dom()
    }
}
