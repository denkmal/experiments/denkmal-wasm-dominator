use std::rc::Rc;

use dominator::Dom;
use futures_signals::signal::SignalExt;
use futures_signals::signal_vec::{MutableVec, SignalVecExt, SignalVec};

use crate::state::*;
use crate::util::*;

#[derive(Debug)]
pub struct Day {
    pub events: MutableVec<Rc<Event>>,
}

impl Day {
    fn events_signal(self: &Rc<Self>, root: &Rc<RootState>) -> impl SignalVec<Item=Rc<Event>> {
        root.event_sorting.signal()
            .switch_signal_vec({
                let day = Rc::clone(self);
                move |sorting| {
                    day.events
                        .signal_vec_cloned()
                        .map_signal(|x| {
                            x.starred.signal().map(move |starred_now| {
                                Rc::new(EventSorter {
                                    event: x.clone(),
                                    starred: starred_now,
                                })
                            })
                        })
                        .sort_by_cloned(move |left: &Rc<EventSorter>, right: &Rc<EventSorter>| {
                            sorting.ordering(left, right)
                        })
                        .map(|event_sorter| {
                            event_sorter.event.clone()
                        })
                }
            })
    }

    pub fn render(self: &Rc<Self>, root: Rc<RootState>) -> Dom {
        html("section")
            .class("swiper-slide")
            .children(&mut [
                html("div")
                    .class("DayEvents-scrollContainer")
                    .children(&mut [
                        html("div")
                            .class("DayEvents")
                            .children(&mut [
                                html("div")
                                    .children(&mut [
                                        html("ul")
                                            .class("DayEvents-list")
                                            .children_signal_vec(
                                                self.events_signal(&root)
                                                    .map(move |event| {
                                                        html("li")
                                                            .class("DayEvents-list-item")
                                                            .children(&mut [
                                                                event.render(),
                                                            ])
                                                            .into_dom()
                                                    })
                                            )
                                            .into_dom(),
                                    ]
                                    )
                                    .into_dom(),
                            ]
                            )
                            .into_dom(),
                    ]
                    )
                    .into_dom(),
            ]
            )
            .into_dom()
    }
}
