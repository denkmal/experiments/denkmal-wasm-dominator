use std::rc::Rc;

use dominator::Dom;

use crate::state::*;
use crate::util::*;

#[derive(Debug)]
pub struct Region {
    pub slug: String,
    pub days: Vec<Rc<Day>>,
}

impl Region {
    pub fn render(&self, root: &Rc<RootState>) -> Dom {
        html("div")
            .class("EventListWeek-swiper")
            .children(&mut [
                html("div")
                    .class("swiper-container")
                    .children(&mut [
                        html("div")
                            .class("swiper-wrapper")
                            .children(self.days.iter()
                                .map(|day| {
                                    let root = Rc::clone(&root);
                                    day.render(root)
                                })
                            )
                            .into_dom()
                    ])
                    .into_dom(),
            ])
            .into_dom()
    }
}
