use std::cmp::Ordering;
use std::collections::HashMap;
use std::rc::Rc;

use dominator::{Dom, events, text};
use futures_signals::signal::{Mutable, SignalExt};
use futures_signals::signal_vec::MutableVec;

use crate::api::*;
use crate::state::*;
use crate::util::*;
use std::fmt;

pub struct RootState {
    counter: Mutable<u64>,
    path: Mutable<String>,

    pub event_data: HashMap<String, Region>,
    pub region_slug: Mutable<String>,
    pub event_sorting: Mutable<EventSorting>,
}

impl Default for RootState {
    fn default() -> Self {
        let api_data: ResponseData = serde_json::from_str(&include_str!("../api.json")).unwrap();
        let regions = api_data.regions.into_iter()
            .map(|region| {
                let slug = region.slug;
                let days = region.events.into_iter()
                    .map(|events| {
                        let events = events.into_iter()
                            .map(|event| {
                                Rc::new(Event {
                                    event,
                                    starred: Mutable::new(false),
                                })
                            })
                            .collect();
                        Rc::new(Day {
                            events: MutableVec::new_with_values(events),
                        })
                    })
                    .collect();
                (slug.clone(), Region { slug, days })
            })
            .collect();

        let region = "tsri".to_string();
        RootState {
            counter: Mutable::new(0),
            path: Mutable::new("/".to_string()),
            region_slug: Mutable::new(region),
            event_data: regions,
            event_sorting: Mutable::new(EventSorting::default()),
        }
    }
}

impl RootState {
    pub fn new(path: String) -> Self {
        Self {
            path: Mutable::new(path),
            ..Default::default()
        }
    }

    pub fn set_path(&self, path: String) {
        if &*path == "/tsri" || &*path == "/basel" {
            let region = path[1..].to_string();
            self.set_region(region)
        }

        self.path.set(path);
        self.incr_counter();
    }

    pub fn incr_counter(&self) {
        self.counter.replace_with(|x| *x + 1);
    }

    pub fn set_region(&self, region: String) {
        self.region_slug.set(region);
    }

    pub fn set_event_sorting(&self, event_sorting: EventSorting) {
        self.event_sorting.set(event_sorting);
    }

    pub fn render(self: &Rc<Self>) -> Dom {
        html("div")
            .children(&mut [
                html("nav")
                    .children(&mut [
                        html("a").attribute("href", "/").text("Index").into_dom(),
                        text(" "),
                        html("a").attribute("href", "/tsri").text("tsri").into_dom(),
                        text(" "),
                        html("a").attribute("href", "/basel").text("basel").into_dom(),
                        text(" "),
                    ])
                    .into_dom(),
                html("div")
                    .text_signal(self.path.signal_cloned().map(|x| format!("Path: {}", x)))
                    .into_dom(),
                html("div")
                    .text_signal(self.counter.signal_cloned().map(|x| format!("Counter: {}", x)))
                    .into_dom(),
                html("div")
                    .children(EventSorting::all().into_iter()
                        .map(move |sorting: EventSorting| {
                            html("button")
                                .text(&sorting.to_string())
                                .attribute_signal("style", self.event_sorting.signal().map(move |current_sorting| {
                                    if current_sorting == sorting { "text-decoration: underline" } else { "" }
                                }))
                                .event({
                                    let state = Rc::clone(&self);
                                    move |_event: events::Click| {
                                        state.set_event_sorting(sorting)
                                    }
                                })
                                .into_dom()
                        })
                    )
                    .into_dom(),
                html("hr")
                    .into_dom(),
                html("div")
                    .children_signal_vec(
                        self.path.signal_ref({
                            let state = Rc::clone(&self);
                            move |path| {
                                vec!(state.render_page(&path))
                            }
                        }).to_signal_vec()
                    )
                    .into_dom(),
            ])
            .into_dom()
    }

    fn render_page(self: &Rc<Self>, path: &String) -> Dom {
        match path.as_str() {
            "/" => {
                html("div").text("Index route").into_dom()
            }
            "/tsri" | "/basel" => {
                // Not using a signal for 'region_slug', because 'path' and 'region_slug' always change together.
                // Maybe the 'region_slug' should be stored _in_ the path enum?
                let slug = self.region_slug.get_cloned();
                let region = self.event_data.get(&slug).unwrap();
                wasm_bindgen_futures::spawn_local(async {
                    createSwiper();
                });
                region.render(self)
            }
            _ => {
                html("div").text("Unknown route").into_dom()
            }
        }
    }
}


#[derive(Copy, Clone, Debug, PartialEq)]
pub enum EventSorting {
    Alphabetical,
    Genre,
    Starred,
}

impl EventSorting {
    pub fn ordering(&self, left: &EventSorter, right: &EventSorter) -> Ordering {
        let ordering = match self {
            EventSorting::Alphabetical => {
                left.event.event.venue.name.cmp(&right.event.event.venue.name)
            }
            EventSorting::Genre => {
                right.event.event.genres.first().map(|g| &g.category.color).cmp(
                    &left.event.event.genres.first().map(|g| &g.category.color)
                ).then_with(move || right.event.event.genres.first().map(|g| &g.name).cmp(
                    &left.event.event.genres.first().map(|g| &g.name))
                )
            }
            EventSorting::Starred => {
                right.starred.cmp(&left.starred)
                    .then_with(move || left.event.event.venue.name.cmp(&right.event.event.venue.name))
            }
        };
        ordering.then_with(move || left.event.event.id.cmp(&right.event.event.id))
    }

    pub fn all() -> Vec<Self> {
        vec!(
            EventSorting::Alphabetical,
            EventSorting::Genre,
            EventSorting::Starred,
        )
    }
}

impl Default for EventSorting {
    fn default() -> Self {
        EventSorting::Alphabetical
    }
}

impl fmt::Display for EventSorting {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            EventSorting::Alphabetical => write!(f, "Alphabetical"),
            EventSorting::Genre => write!(f, "Genre"),
            EventSorting::Starred => write!(f, "Starred"),
        }
    }
}
