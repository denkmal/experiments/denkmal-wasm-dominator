use serde::{Deserialize, Serialize};

#[allow(dead_code)]
type Boolean = bool;
#[allow(dead_code)]
type Float = f64;
#[allow(dead_code)]
type Int = i64;
#[allow(dead_code)]
type ID = String;
#[doc = "ISO 8601 date string"]
type Date = String;
#[doc = "Date with format: YYYY-MM-DD"]
type EventDay = String;
#[doc = "URL string"]
type URL = String;
#[doc = "An RFC 4122 compatible UUID String"]
type UUID = String;

#[derive(Clone, Debug, Deserialize, PartialEq)]
pub struct GetEventsRegionsEventsLinks {
    pub label: String,
    pub url: URL,
}

#[derive(Clone, Debug, Deserialize, PartialEq)]
pub struct GetEventsRegionsEventsGenresCategory {
    pub color: Option<String>,
}

#[derive(Clone, Debug, Deserialize, PartialEq)]
pub struct GetEventsRegionsEventsGenres {
    pub name: String,
    pub category: GetEventsRegionsEventsGenresCategory,
}

#[derive(Clone, Debug, Deserialize, PartialEq)]
pub struct GetEventsRegionsEventsVenueRegion {
    pub slug: String,
    #[serde(rename = "dayOffset")]
    pub day_offset: Float,
    #[serde(rename = "timeZone")]
    pub time_zone: String,
}

#[derive(Clone, Debug, Deserialize, PartialEq)]
pub struct GetEventsRegionsEventsVenue {
    pub id: UUID,
    pub name: String,
    pub address: Option<String>,
    pub url: Option<String>,
    #[serde(rename = "facebookPageId")]
    pub facebook_page_id: Option<String>,
    pub latitude: Option<Float>,
    pub longitude: Option<Float>,
    pub region: GetEventsRegionsEventsVenueRegion,
}

#[derive(Clone, Debug, Deserialize, PartialEq)]
pub struct GetEventsRegionsEvents {
    pub id: UUID,
    #[serde(rename = "isPromoted")]
    pub is_promoted: Boolean,
    pub description: String,
    #[serde(rename = "hasTime")]
    pub has_time: Boolean,
    pub from: Date,
    pub until: Option<Date>,
    #[serde(rename = "eventDay")]
    pub event_day: EventDay,
    pub links: Vec<GetEventsRegionsEventsLinks>,
    pub genres: Vec<GetEventsRegionsEventsGenres>,
    pub tags: Vec<String>,
    pub venue: GetEventsRegionsEventsVenue,
}

#[derive(Clone, Debug, Deserialize, PartialEq)]
pub struct GetEventsRegions {
    pub slug: String,
    pub events: Vec<Vec<GetEventsRegionsEvents>>,
}

#[derive(Clone, Debug, PartialEq, Serialize)]
pub struct Variables {
    #[serde(rename = "eventDays")]
    pub event_days: Vec<EventDay>,
}

impl Variables {}

#[derive(Clone, Debug, Deserialize, PartialEq)]
pub struct ResponseData {
    pub regions: Vec<GetEventsRegions>,
}
