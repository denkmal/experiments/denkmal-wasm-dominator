denkmal-wasm-dominator
======================

Experimental WebAssembly client for Denkmal.org written in Rust using FRP-signals.

Features:
- Based on the experimental FRP library [rust-dominator](https://github.com/Pauan/rust-dominator).
- Routing using the _History_ API
- List of events, clicking on an event sets it as 'favorited'.
- Configurable sorting of the events list.

![](docs/screenshots/screenshot.png)


Development
-----------

### Setup

Install cargo-make:
```
cargo install cargo-make
```

Add the "wasm" build target:
```
rustup target add wasm32-unknown-unknown
```

### Build and Serve
Start a webserver at http://localhost:8000
```
cargo make serve
```

Watch for file changes and build:
```
cargo make watch
```
