const jsdom = require('jsdom')
const fs = require('fs')
const {Script} = require('vm')

async function run () {
  const dom = new jsdom.JSDOM(`<div id="app_mount"></div>`, {runScripts: 'outside-only'})
  const vmContext = dom.getInternalVMContext()
  const runScript = async (js) => (new Script(js)).runInContext(vmContext)

  // Load the application JS bundle
  await runScript(fs.readFileSync('./dist/index.js'))

  // Increase stack trace limit for more useful error messages
  await runScript('Error.stackTraceLimit = 100')

  // Workaround for https://github.com/jsdom/jsdom/issues/2740
  // This is needed for wasm_bindgen's [web_sys::window()] to work.
  await runScript('Object.setPrototypeOf(window, Window.prototype)')

  // Get reference to the dominator application
  const app = await runScript('wasm_ssr.instantiateApp()')

  // Render different application states
  const paths = [
    '/',
    '/tsri',
  ]
  for (let path of paths) {
    app.set_path(path)
    await sleep(0)
    let html = dom.serialize()
    console.log(`- ${path}: ${html.substring(0, 1000)}`);
  }
}

async function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

run()
