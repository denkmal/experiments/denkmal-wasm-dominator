import rust from '@wasm-tool/rollup-plugin-rust'
import inject from '@rollup/plugin-inject'
import resolve from '@rollup/plugin-node-resolve'
import commonjs from '@rollup/plugin-commonjs'

export default {
  input: {
    index: 'rollup-input.js'
  },
  output: {
    dir: 'dist',
    name: 'wasm_ssr',
    format: 'iife'
  },
  plugins: [
    rust({
      debug: true,
      inlineWasm: true,
    }),

    // Polyfill `TextDecoder` which is not available in 'jsdom'.
    inject({
      TextDecoder: ['text-encoding', 'TextDecoder'],
      TextEncoder: ['text-encoding', 'TextEncoder']
    }),
    commonjs(),
    resolve(),
  ],
}
