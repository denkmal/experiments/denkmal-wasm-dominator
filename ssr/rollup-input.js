import wasm from '../Cargo.toml'

export async function instantiateApp () {
  const module = await wasm()
  return new (module).Client()
}
